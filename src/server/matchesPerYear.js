//function matchesPerYear(matches){
//    var obj={};
//    var len=matches.length;
//    for(var i=0;i<len;i++){
//        if(obj[matches[i].season]===undefined){
//            obj[matches[i].season]=1;   
//        }else{
//            obj[matches[i].season]+=1;
//        }
//    }
//    //console.log(obj);
//    return obj;
//}
function matchesPerYear(matches){
    let matchesPerYear = matches.reduce(function(result,value){
        if(result[value.season] === undefined){
            result[value.season] = 1
        }
        if(result[value.season]){
            result[value.season]+=1  
        }else{
            result[value.season] +=1
        }
        return result
    },{});
    return matchesPerYear
}

module.exports = matchesPerYear;