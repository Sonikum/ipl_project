function extra_run(deliveries, matches) {
    //console.log(deliveries[0]);
    let obj={};
    for(let i=0;i<matches.length;i++){
        if(matches[i].season === "2016"){
            for(let j=0;j<deliveries.length;j++){
                if(matches[i].id === deliveries[j].match_id){
                    bowling=deliveries[j].bowling_team;
                    if(obj[bowling]===undefined){
                        obj[bowling]=Number(deliveries[j].extra_runs);
                    }else{
                        obj[bowling]+=Number(deliveries[j].extra_runs);
                    }

                }
            }
        }
    } 
    return obj
}
module.exports = extra_run;